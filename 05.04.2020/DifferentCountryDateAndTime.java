import java.time.LocalTime;
import java.time.ZoneId;

import java.time.ZonedDateTime;
import java.time.*;

public class DifferentCountryDateAndTime {
	public static void main(String args[]) {
		
		ZoneId zoneIdCanada = ZoneId.of("Canada/Yukon");
		LocalTime canadaTime = LocalTime.now(zoneIdCanada);
		System.out.println("Canada Time is : " + canadaTime);
		
		ZoneId zoneIdBrazil = ZoneId.of("Brazil/DeNoronha");
		LocalTime brazilTime = LocalTime.now(zoneIdBrazil);
		System.out.println("brazil Time is : " + brazilTime);
		
		
		   // just a try 
		    ZoneId  brazil = ZoneId.of("Brazil/DeNoronha");   
		    ZonedDateTime zone1  = ZonedDateTime.now(brazil);   
		    System.out.println("In brazil Time Zone: " + zone1); 
		    
		    ZoneId  canada = ZoneId.of("Canada/Yukon");   
		    ZonedDateTime zone2   = zone1.withZoneSameInstant(canada);   
		    System.out.println("In Canada Time Zone:"  + zone2);  
		
	

		
	}
/*
Canada Time is : 22:40:49.293
brazil Time is : 03:40:49.319
In brazil Time Zone: 2020-04-06T03:40:49.320-02:00[Brazil/DeNoronha]
In Canada Time Zone:2020-04-05T22:40:49.320-07:00[Canada/Yukon]
 */
}