package bcas.prog.str;

public class StringMethods {
	public static void main(String []args) {
	
	String index = "I love my family very much ";
	char anwser = index.charAt(10);
	System.out.println(index);
	System.out.println("Test 1: String charAt() Method " );
	System.out.println( anwser);
	System.out.println("__________________________________________________");

	
	String firstName = " Dilukshika ";
	String lastName = " Sivanathan ";
	String space = " ";
	String firstNameAndLastName = firstName.concat(space).concat(lastName);
	System.out.println(firstName);
	System.out.println(lastName);
	System.out.println("Test 5: String concat() Method ");
	System.out.println(firstNameAndLastName);
	System.out.println("__________________________________________________");
	
	
	String ch = new String ("The love of a family is life's greatest blessing");
	System.out.println(ch);
	System.out.println("Test 16: int indexOf() Method " );
	System.out.println("Find Index = "+ ch.indexOf("e"));
	System.out.println("__________________________________________________");
	
	
	String Str = new String("The love of a family is life's greatest blessing");
	System.out.println(Str);
    System.out.println("Test 17: String indexOf(char ch, int fromIndex) Method" );
    System.out.println("Found Index = "+ Str.indexOf( 'e', 3 ));
    System.out.println("__________________________________________________");
    
    
    String Stri = new String("The love of a family is life's greatest blessing");
    String SubStr = new String("blessing");
    System.out.println(Stri);
    System.out.println(SubStr);
    System.out.println("Test 18: String indexOf(String str) Method ");
    System.out.println("Found Index = " + Stri.indexOf( SubStr ));
    System.out.println("__________________________________________________");
    
    
    String fromIndex  = new String("The love of a family is life's greatest blessing");
    String SubStr1 = new String("greatest" );
    System.out.println(fromIndex);
    System.out.println(SubStr1);
    System.out.println("Test 19: String indexOf(String str, int fromIndex) Method" );
    System.out.println( "Found Index = " + Str.indexOf( SubStr1, 50 ));
    System.out.println("__________________________________________________");
    
    
    String lastIndex = new String("The love of a family is life's greatest blessing");
    System.out.println(lastIndex);
    System.out.println("Test 21: String lastIndexOf(int ch) Method");
    System.out.println("Found Last Index = " + Str.lastIndexOf( 't' ));
    System.out.println("__________________________________________________");
    
    
    String lastIndex1 = new String("The love of a family is life's greatest blessing");
    System.out.println(lastIndex1);
    System.out.println("Test 22: String lastIndexOf(int ch, int fromIndex) Method");
    System.out.println("Found Last Index = " + Str.lastIndexOf( 'e', 3 ));
    System.out.println("__________________________________________________");
    
    
    String lastIndex2 = new String("The love of a family is life's greatest blessing");
    String SubStr2 = new String("family" );
    System.out.println(lastIndex2);
    System.out.println(SubStr2);
    System.out.println("Test 23: String lastIndexOf(String str) Method" );
    System.out.println( "Found Last Index = "+ Str.lastIndexOf( SubStr2 ));
    System.out.println("__________________________________________________");
  
    
    String lastIndex3 = new String("The love of a family is life's greatest blessing");
    String SubStr3 = new String("family" );
    System.out.println(lastIndex3);
    System.out.println(SubStr3);
    System.out.println("Test 24: String lastIndexOf(String str, int fromIndex) Method" );
    System.out.println( "Found Last Index = " + Str.lastIndexOf( SubStr1, 20 ));
    System.out.println("__________________________________________________");
   
    
    String length1 = new String("The love of a family is life's greatest blessing");
    String length2 = new String("greatest" );
    System.out.println(length1);
    System.out.println(length2);
    System.out.println("Test 25: String  length() Method");
    System.out.println( "String Length = " + length1.length());
    System.out.println("String Length = " +  length2.length());
    System.out.println("__________________________________________________");
    
    
    String replace = new String("The love of a family is life's greatest blessing");
    System.out.println(replace);
    System.out.println("Test 29: String replace(char oldChar, char newChar) Method" );
    System.out.println("Return Value = " + Str.replace('a', 'T'));
    System.out.println("Return Value = " + Str.replace('i', 'D'));
    System.out.println("__________________________________________________");
    
    
    String booleanWith = new String("The love of a family is life's greatest blessing");
    System.out.println(booleanWith);
    System.out.println("Test 34: String boolean startsWith(String prefix) Method" );
    System.out.println("Return Value = " + Str.startsWith("The"));
    System.out.println("Return Value = " + Str.startsWith("This"));
    System.out.println("__________________________________________________");
  
    
    String StrSubStr = new String("The love of a family is life's greatest blessing");
    System.out.println(StrSubStr);
    System.out.println("Test 37: String substring(int beginIndex) Method" );
    System.out.println( "Return Value = " + Str.substring(20));
    System.out.println("__________________________________________________");
    
    
    String StrSubStr1 = new String("The love of a family is life's greatest blessing");
    System.out.println(StrSubStr);
    System.out.println("Test 38: String substring(int beginIndex, int endIndex) Method" );
    System.out.println( "Return Value = " + StrSubStr.substring(14, 30));
    System.out.println("__________________________________________________");
    
    
    String StrLowerCase = new String("The love of a family is life's greatest blessing");
    System.out.println(StrLowerCase);
    System.out.println("Test 40: String toLowerCase() Method");
    System.out.println("Return Value = " + StrLowerCase.toLowerCase());
    System.out.println("__________________________________________________");

    
    String StrUpperCase = new String("The love of a family is life's greatest blessing");
    System.out.println(StrUpperCase);
    System.out.println("Test 43: String toUpperCase() Method");
    System.out.println("Return Value = " +  StrUpperCase.toUpperCase() );
    System.out.println("__________________________________________________");
    
    
    String StrTrim = new String("   The love of a family is life's greatest blessing   ");
    System.out.println(StrTrim);
    System.out.println("Test 45: String trim() Method");
    System.out.println("Return Value =" + StrTrim.trim());
    System.out.println("__________________________________________________");
    
    
	}
}

/*
 output
 
I love my family very much 
Test 1: String charAt() Method 
f
__________________________________________________
Dilukshika 
Sivanathan 
Test 5: String concat() Method 
Dilukshika   Sivanathan 
__________________________________________________
The love of a family is life's greatest blessing
Test 16: int indexOf() Method 
Find Index = 2
__________________________________________________
The love of a family is life's greatest blessing
Test 17: String indexOf(char ch, int fromIndex) Method
Found Index = 7
__________________________________________________
The love of a family is life's greatest blessing
blessing
Test 18: String indexOf(String str) Method 
Found Index = 40
__________________________________________________
The love of a family is life's greatest blessing
greatest
Test 19: String indexOf(String str, int fromIndex) Method
Found Index = -1
__________________________________________________
The love of a family is life's greatest blessing
Test 21: String lastIndexOf(int ch) Method
Found Last Index = 38
__________________________________________________
The love of a family is life's greatest blessing
Test 22: String lastIndexOf(int ch, int fromIndex) Method
Found Last Index = 2
__________________________________________________
The love of a family is life's greatest blessing
family
Test 23: String lastIndexOf(String str) Method
Found Last Index = 14
__________________________________________________
The love of a family is life's greatest blessing
family
Test 24: String lastIndexOf(String str, int fromIndex) Method
Found Last Index = -1
__________________________________________________
The love of a family is life's greatest blessing
greatest
Test 25: String  length() Method
String Length = 48
String Length = 8
__________________________________________________
The love of a family is life's greatest blessing
Test 29: String replace(char oldChar, char newChar) Method
Return Value = The love of T fTmily is life's greTtest blessing
Return Value = The love of a famDly Ds lDfe's greatest blessDng
__________________________________________________
The love of a family is life's greatest blessing
Test 34: String boolean startsWith(String prefix) Method
Return Value = true
Return Value = false
__________________________________________________
The love of a family is life's greatest blessing
Test 37: String substring(int beginIndex) Method
Return Value =  is life's greatest blessing
__________________________________________________
The love of a family is life's greatest blessing
Test 38: String substring(int beginIndex, int endIndex) Method
Return Value = family is life's
__________________________________________________
The love of a family is life's greatest blessing
Test 40: String toLowerCase() Method
Return Value = the love of a family is life's greatest blessing
__________________________________________________
The love of a family is life's greatest blessing
Test 43: String toUpperCase() Method
Return Value = THE LOVE OF A FAMILY IS LIFE'S GREATEST BLESSING
__________________________________________________
   The love of a family is life's greatest blessing   
Test 45: String trim() Method
Return Value =The love of a family is life's greatest blessing
__________________________________________________

 */
