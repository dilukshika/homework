import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

public class DateAndTimeDemo {
	public static void main(String args[]) {
		System.out.println("Current Date And Time");
		
		LocalDate date = LocalDate .now();
		System.out.println(date);
		
		LocalTime time = LocalTime.now();
		System.out.println(time);
		
		LocalDateTime dateTime = LocalDateTime.now();
		System.out.println(dateTime);
		
		ZoneId zoneIdSin = ZoneId.of("Singapore");
		LocalTime singaporeTime = LocalTime.now(zoneIdSin);
		System.out.println("Singapore Time is : " + singaporeTime);
		
		
		
		
	}

}
